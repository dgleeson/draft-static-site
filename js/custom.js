

document.onscroll = function() {
    if( $(window).scrollTop() > $('#logo').height() ) {
        $('nav-top').removeClass('navbar-static-top').addClass('navbar-fixed-top');
    }
    else {
        $('nav-top').removeClass('navbar-fixed-top').addClass('navbar-static-top');
    }
};